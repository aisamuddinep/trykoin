package com.isam.trykoin.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}