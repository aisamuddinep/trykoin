package com.isam.trykoin.network

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1)
}