package com.isam.trykoin.network

import retrofit2.HttpException
import java.net.SocketTimeoutException

open class ResponHandler {
    fun <T: Any> handleSuccess(data: T): Resources<T>{
        return Resources.success(data)
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            ErrorCodes.SocketTimeOut.code -> "Timeout"
            401 -> "Unauthorised"
            404 -> "Not found"
            else -> "Something went wrong"
        }
    }

    fun <T: Any> handleException(e: Exception): Resources<T>{
        return when (e){
            is HttpException -> Resources.error(getErrorMessage(e.code()), null)
            is SocketTimeoutException -> Resources.error(getErrorMessage(ErrorCodes.SocketTimeOut.code), null)
            else -> Resources.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }
}