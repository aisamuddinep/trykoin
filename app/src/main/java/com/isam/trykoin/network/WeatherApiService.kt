package com.isam.trykoin.network

import com.isam.trykoin.model.Weather
import retrofit2.http.GET

interface WeatherApiService {
    @GET("weather?q=Jakarta&units=metric")
    suspend fun getForecast(): Weather
}