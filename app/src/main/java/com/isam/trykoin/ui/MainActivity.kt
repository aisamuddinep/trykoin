package com.isam.trykoin.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.isam.trykoin.R
import com.isam.trykoin.preferences.ForecastPreferences
import com.isam.trykoin.ui.forecast.ForecastFragment
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {
    private val preferences: ForecastPreferences by inject()
    private val forecastFragment: ForecastFragment by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        if (preferences.getShouldShowFragment()) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.clRoot, forecastFragment, ForecastFragment.FORECAST)
                .commit()
        }
    }
}