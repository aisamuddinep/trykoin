package com.isam.trykoin.ui.forecast

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.isam.trykoin.R
import com.isam.trykoin.databinding.FragmentForecastBinding
import com.isam.trykoin.model.Weather
import com.isam.trykoin.network.Resources
import com.isam.trykoin.network.Status
import org.koin.androidx.viewmodel.ext.android.viewModel

class ForecastFragment : Fragment() {

    companion object{
        const val FORECAST = "forecast"
    }

    private val forecastViewModel: ForecastViewModel by viewModel()
    private lateinit var binding: FragmentForecastBinding

    private val observer = Observer<Resources<Weather>> {weather ->
        when (weather.status) {
            Status.SUCCESS -> updateTemperatureText(weather.data?.name, weather.data?.temp?.temp.toString())
            Status.ERROR -> weather.message?.let { it -> showError(it) }
            Status.LOADING -> showLoading()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forecast, container, false)
        forecastViewModel.weather.observe(viewLifecycleOwner, observer)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun showLoading() {
        binding.tvForecastInfo.text = "Loading..."
    }

    @SuppressLint("SetTextI18n")
    private fun showError(message: String) {
        binding.tvForecastInfo.text = "Error: $message"
    }

    private fun updateTemperatureText(name: String?, temp: String?) {
        binding.tvForecastInfo.text = getString(R.string.temp_info, name, temp)
    }
}