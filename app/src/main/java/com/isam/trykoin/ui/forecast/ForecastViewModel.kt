package com.isam.trykoin.ui.forecast

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.isam.trykoin.model.Weather
import com.isam.trykoin.network.Resources
import com.isam.trykoin.repository.WeatherRepository
import kotlinx.coroutines.Dispatchers

class ForecastViewModel(private val weatherRepository: WeatherRepository): ViewModel() {

    val weather: LiveData<Resources<Weather>> = liveData(Dispatchers.IO) {
        emit(Resources.loading(null))
        emit(weatherRepository.getWeather())
    }

}