package com.isam.trykoin.repository

import com.isam.trykoin.model.Weather
import com.isam.trykoin.network.Resources
import com.isam.trykoin.network.ResponHandler
import com.isam.trykoin.network.WeatherApiService

open class WeatherRepository(
    private val weatherApiService: WeatherApiService,
    private val responHandler: ResponHandler
) {

    suspend fun getWeather(): Resources<Weather> {
        return try {
            responHandler.handleSuccess(weatherApiService.getForecast())
        } catch (e: Exception) {
            responHandler.handleException(e)
        }
    }

}