package com.isam.trykoin.preferences

import android.content.Context
import android.content.SharedPreferences

class ForecastPreferences(context: Context) {
    private val preferences: SharedPreferences = context.getSharedPreferences("pref", Context.MODE_PRIVATE)
    private val showFragmentKey = "showFragment"

    init {
        storeShouldShowFragment(true)
    }

    private fun storeShouldShowFragment(shouldShow: Boolean){
        preferences.edit().putBoolean(showFragmentKey, shouldShow).apply()
    }

    fun getShouldShowFragment(): Boolean{
        return preferences.getBoolean(showFragmentKey, false)
    }

}