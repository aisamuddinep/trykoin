package com.isam.trykoin

import android.app.Application
import com.isam.trykoin.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TryKoinApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidLogger()
            androidContext(this@TryKoinApplication)
            modules(appComponent)
        }
    }
}