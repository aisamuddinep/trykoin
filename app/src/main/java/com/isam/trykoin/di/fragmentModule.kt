package com.isam.trykoin.di

import com.isam.trykoin.ui.forecast.ForecastFragment
import org.koin.dsl.module

val fragmentModule = module {
    factory { ForecastFragment() }
}