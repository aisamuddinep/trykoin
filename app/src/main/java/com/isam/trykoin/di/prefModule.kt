package com.isam.trykoin.di

import com.isam.trykoin.preferences.ForecastPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val prefModule = module {
    single {ForecastPreferences(androidContext())}
}