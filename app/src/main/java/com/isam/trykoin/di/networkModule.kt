package com.isam.trykoin.di

import com.isam.trykoin.network.*
import org.koin.dsl.module

val networkModule = module {
    factory { AuthInterceptor() }
    factory { provideOkHttpClient(get(),get()) }
    factory { provideForecastApi(get()) }
    factory { ResponHandler() }
    factory { provideLoggingInterceptor() }
    single { provideRetrofit(get()) }
}