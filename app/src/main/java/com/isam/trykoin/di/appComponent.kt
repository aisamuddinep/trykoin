package com.isam.trykoin.di

val appComponent = listOf(
    prefModule,
    fragmentModule,
    viewModelModule,
    networkModule,
    repositoryModule
)