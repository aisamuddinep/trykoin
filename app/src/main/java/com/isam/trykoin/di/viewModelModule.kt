package com.isam.trykoin.di

import com.isam.trykoin.ui.forecast.ForecastViewModel
import org.koin.dsl.module

val viewModelModule = module {
    factory { ForecastViewModel(get()) }
}