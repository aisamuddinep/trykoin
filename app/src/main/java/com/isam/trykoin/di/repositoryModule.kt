package com.isam.trykoin.di

import com.isam.trykoin.repository.WeatherRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory { WeatherRepository(get(), get()) }
}