package com.isam.trykoin.model

data class TempData (
    val temp: Double,
    val humidity: Int
)