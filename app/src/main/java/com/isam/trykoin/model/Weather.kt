package com.isam.trykoin.model

import com.google.gson.annotations.SerializedName

data class Weather (
    @SerializedName("main")
    val temp: TempData,
    val name: String
)